package com.xuecheng.framework.domain.cms.response;

import com.xuecheng.framework.domain.cms.constants.Constants;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @ClassName : EnvisionResponse
 * @Description : 统一响应类
 * @Auther : zhangyan
 * @Date :  2019/02/20 20:18
 */
public class EnvisionResponse<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String SUCCESS = Constants.SUCCESS;

    private String code;
    private String message;
    private T data;

    public EnvisionResponse() {
    }

    public EnvisionResponse(String errCode, String errorMessage) {
        this.code = errCode;
        this.message = errorMessage;
    }

    public EnvisionResponse(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public boolean isSuccess() {
        return StringUtils.equals(this.getCode(), SUCCESS);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
