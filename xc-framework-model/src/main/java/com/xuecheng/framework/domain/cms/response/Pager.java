package com.xuecheng.framework.domain.cms.response;

import java.io.Serializable;
import java.util.List;

public class Pager<T> implements Serializable {
    private static final long serialVersionUID = -6132320072158319161L;
    private long totalCount;                              // 总记录数
    private int pageNo;                                     //页码
    private List<T> data;                                    // 分页数据
    private List<Integer> dataList;

    public Pager() {
    }

    public Pager(long totalCount, List<T> data) {
        this.totalCount = totalCount;
        this.data = data;
    }

    public Pager(int pageNo, long totalCount, List<T> data) {
        this.pageNo = pageNo;
        this.totalCount = totalCount;
        this.data = data;
    }

    public Pager(int pageNo, long totalCount, List<T> data, List<Integer> dataList) {
        this.pageNo = pageNo;
        this.totalCount = totalCount;
        this.data = data;
        this.dataList = dataList;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public List<Integer> getDataList() {
        return dataList;
    }

    public void setDataList(List<Integer> dataList) {
        this.dataList = dataList;
    }
}
