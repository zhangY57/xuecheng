package com.xuecheng.framework.domain.cms.request;

import lombok.Data;

/**
 * @ClassName : PageQuery
 * @Description : page查询条件
 * @Auther : zhangyan
 * @Date :  2019/02/20 22:13
 */
@Data
public class PageQuery {

    /**
     * 站点
     */
    private String siteId;
    /**
     * 页面ID
     */
    private String pageId;
    /**
     * 页面名称
     */
    private String pageName;
    /**
     * 别名
     */
    private String pageAliase;
    /**
     * 模版id
     */
    private String templateId;
}
