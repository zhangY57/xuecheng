package com.xuecheng.framework.domain.cms.constants;

/**
 * @ClassName : Constants
 * @Description : 统一响应码
 * @Auther : zhangyan
 * @Date :  2019/02/20 20:21
 */
public class Constants {
    public static final boolean TRUE = true;

    public static final boolean FALSE            = false;
    /**
     * 数据库中is_开头(如is_deleted,is_visible)通用常量值
     */
    public static final int     DB_IS_STATUS_YES = 1;
    public static final int     DB_IS_STATUS_NO  = 0;
    /**
     * 成功码
     */
    public static final String SUCCESS = "200";

    /**
     * 参数空错误码
     */
    public static final String NULLCODE  = "400";
    /**
     * 参数校验错误码
     */
    public static final String PARAMCODE = "405";

    /**
     * 错误码
     */
    public static final String ERRCODE    = "500";
    /**
     * 系统错误
     */
    public static final String SYSERRCODE = "504";

    /**
     * 未登录
     */
    public static final String NOLOGIN = "1001";

    /**
     * 没有权限
     */
    public static final String NOAUTH = "1002";

    /**
     * 被登陆
     */
    public static final String DUPLICATE_LOGIN = "1003";

    /**
     * 错误信息
     */
    public static final String ERRMESS          = "请求出错，请稍后重试";
    /**
     * 手机号校验正则
     */
    public static final String PHONE_REGEX      = "^[1][0-9]{10}$";
    /**
     * 两位小数校验正则表达书
     */
    public static final String DOUBEL_TWO_REGEX = "^[0-9]+(.[0-9]{1,2})?$";

    /**
     * 新增用户默认密码
     */
    public static final String DEFAULT_PWD = "abc123";

    /**
     * redission的token中信息的map集合
     */
    public static final String REDISSION_TOKEN_INFOS_MAP = "yj-auth-token-info";

    /**
     * redission中token中key的map集合
     */
    public static final String REDISSION_TOKEN_KEYS_MAP = "yj-auth-token-key";

    /**
     * token
     */
    public static final String  APP_TOKEN                     = "token";
}

