package com.xuecheng.test.rabbit;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName : RabbitPulishProducerTest
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/13 22:21
 */
public class RabbitPulishProducerTest {
    private static final String QUEUE_INFORM_EMAIL = "queue_inform_email";
    private static final String QUEUE_INFORM_SMS = "queue_inform_sms";
    private static final String EXCHANGE_FANOUT_INFORM="exchange_fanout_inform";
    private static final Logger logger = LoggerFactory.getLogger(RabbitPulishProducerTest.class);
    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        //创建连接对象
        Connection connection = connectionFactory.newConnection();
        //创建连接通道
        Channel channel = connection.createChannel();
        //创建与交换机的连接通道
        channel.exchangeDeclare(EXCHANGE_FANOUT_INFORM, BuiltinExchangeType.FANOUT);
        //声明队列
        channel.queueDeclare(QUEUE_INFORM_EMAIL, true, false, false, null);
        channel.queueDeclare(QUEUE_INFORM_SMS, true, false, false, null);
        //交换机和队列绑定
        channel.queueBind(QUEUE_INFORM_SMS,EXCHANGE_FANOUT_INFORM,"");
        channel.queueBind(QUEUE_INFORM_EMAIL,EXCHANGE_FANOUT_INFORM,"");

//        channel.basicPublish("", QUEUE, null, message.getBytes());
        for(int i = 0;i<5;i++){
            String message = "Hello World!"+i;
            //发送消息
            channel.basicPublish(EXCHANGE_FANOUT_INFORM,"",null,message.getBytes());
            logger.info("发送成功!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }

        //关闭连接
        channel.close();
        connection.close();
    }
}
