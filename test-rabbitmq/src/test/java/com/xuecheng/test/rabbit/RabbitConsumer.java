package com.xuecheng.test.rabbit;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @ClassName : RabbitConsumer
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/12 22:18
 */
public class RabbitConsumer {
    private static final Logger logger = LoggerFactory.getLogger(RabbitConsumer.class);
    private static final String QUEUE = "helloworld";

    public static void main(String[] args) throws Exception {
        //创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        //获取连接对象
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE,true, false, false, null);
        DefaultConsumer consumer = new DefaultConsumer(channel){
            /**
             * 这都是啥玩意
             * @param consumerTag 消费标签
             * @param envelope 消息包的内容，可从中获取消息id，消息routingkey，交换机，消息和重传标志
             * (收到消息失败后是否需要重新发送)
             * @param properties
             * @param body
             * @throws IOException
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                //交换机
                String exchange = envelope.getExchange();
                //路由key
                String routingKey = envelope.getRoutingKey();
                //id
                long deliveryTag = envelope.getDeliveryTag();
                //消息内容
                String string = new String(body,"utf-8");
                logger.info("接收到的内容 交换机:{},路由key:{},消息id:{},内容:{}",exchange,routingKey,deliveryTag,string);
            }
        };
        channel.basicConsume(QUEUE,true,consumer);
    }
}
