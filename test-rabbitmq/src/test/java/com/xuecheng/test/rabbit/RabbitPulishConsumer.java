package com.xuecheng.test.rabbit;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @ClassName : RabbitPulishConsumer
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/13 22:50
 */
public class RabbitPulishConsumer {
    private static final String QUEUE_INFORM_EMAIL = "queue_inform_email";
    private static final String QUEUE_INFORM_SMS = "queue_inform_sms";
    private static final String EXCHANGE_FANOUT_INFORM="exchange_fanout_inform";
    private static final Logger logger = LoggerFactory.getLogger(RabbitPulishConsumer.class);
    public static void main(String[] args) throws Exception {
        //创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        //获取连接对象
        Connection connection = connectionFactory.newConnection();
        //创建连接通道 每个通道代表一个会话
        Channel channel = connection.createChannel();
        //连接通道与交换机连接
        channel.exchangeDeclare(EXCHANGE_FANOUT_INFORM, BuiltinExchangeType.FANOUT);
        //声明队列
        channel.queueBind(QUEUE_INFORM_EMAIL,EXCHANGE_FANOUT_INFORM,"");
        //处理业务
        DefaultConsumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body,"utf-8");
                logger.info("接收到的消息:{}",message);
            }
        };
        //接收消息
        channel.basicConsume(QUEUE_INFORM_EMAIL,true,consumer);

    }
}
