package com.xuecheng.test.publishsubript;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @ClassName : PublishConsumer
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/16 09:38
 */
public class PublishConsumer {
    private static final String QUEUE_SMS = "zhangxu_sms";
    private static final String QUEUE_EMAIL = "zhangxu_email";
    private static final String EXCHANGE_NAME = "zhangxu_email";

    public static void main(String[] args) throws Exception {
        //创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        //创建连接对象
        Connection connection = connectionFactory.newConnection();
        //创建连接通道
        Channel channel = connection.createChannel();
        //声明连接的交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        //声明队列
        channel.queueDeclare(QUEUE_EMAIL,true,false,false,null);
        //绑定
        channel.queueBind(QUEUE_EMAIL,EXCHANGE_NAME,"");
        //接收消息
        DefaultConsumer consumer =  new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String messgae = new String(body,"utf-8");
                System.out.println(messgae);
            }
        };
        channel.basicConsume(QUEUE_EMAIL,consumer);
    }
}
