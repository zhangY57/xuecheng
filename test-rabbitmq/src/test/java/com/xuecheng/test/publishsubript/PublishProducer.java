package com.xuecheng.test.publishsubript;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @ClassName : PublishProducer
 * @Description : 发布/订阅模式测试
 * @Auther : zhangyan
 * @Date :  2019/03/16 08:32
 */
public class PublishProducer {
    private static final String QUEUE_SMS = "zhangxu_sms";
    private static final String QUEUE_EMAIL = "zhangxu_email";
    private static final String EXCHANGE_NAME = "zhangxu_email";

    public static void main(String[] args) throws Exception {
        //创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        //创建连接对象
        Connection connection = connectionFactory.newConnection();
        //创建连接通道
        Channel channel = connection.createChannel();
        //交换机与通道连接
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        //声明队列
        channel.queueDeclare(QUEUE_EMAIL, true, false, false, null);
        channel.queueDeclare(QUEUE_SMS, true, false, false, null);

        //创建通道与交换
        channel.queueBind(QUEUE_EMAIL, EXCHANGE_NAME, "");
        channel.queueBind(QUEUE_SMS, EXCHANGE_NAME, "");
        //发送消息
        for (int i = 0; i < 5; i++) {
            String message = "发布订阅" + System.currentTimeMillis();
            channel.basicPublish(EXCHANGE_NAME,"",null,message.getBytes());
        }
        channel.close();
        connection.close();
    }
}
