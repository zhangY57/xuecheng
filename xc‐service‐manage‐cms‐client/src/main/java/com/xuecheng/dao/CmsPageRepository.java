package com.xuecheng.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * cmspage接口
 */
public interface CmsPageRepository extends MongoRepository<CmsPage,String> {
}
