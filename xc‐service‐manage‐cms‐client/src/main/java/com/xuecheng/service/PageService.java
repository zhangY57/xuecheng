package com.xuecheng.service;

import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.xuecheng.dao.CmsPageRepository;
import com.xuecheng.framework.domain.cms.CmsPage;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * @ClassName : PageService
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/16 19:56
 */
@Service
public class PageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PageService.class);

    @Autowired
    private CmsPageRepository cmsPageRepository;

    @Autowired
    private GridFSBucket gridFSBucket;

    @Autowired
    private GridFsTemplate gridFsTemplate;

    /**
     * 保存页面信息
     *
     * @param pageId
     * @throws Exception
     */
    public void savePageToServerPath(String pageId) {
        if (StringUtils.isBlank(pageId)) {
            return;
        }
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            CmsPage cmsPage = this.findById(pageId);
            if (cmsPage == null) {
                LOGGER.warn("查询页面为空 pageId:{}", pageId);
                return;
            }
            //物理路径
            String pagePhysicalPath = cmsPage.getPagePhysicalPath() + cmsPage.getPageName();
            //html主键
            String htmlFileId = cmsPage.getHtmlFileId();
            inputStream = this.htmlDownload(htmlFileId);
            //下载文件
            File file = new File(pagePhysicalPath);
            fileOutputStream = new FileOutputStream(file);
            IOUtils.copy(inputStream, fileOutputStream);
        } catch (IOException e) {
            LOGGER.error("保存页面信息错误:{}", e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 获取输入流
     */
    public InputStream htmlDownload(String htmlFileId) {
        try {
            GridFSFile gridFSFile = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(htmlFileId)));
            //打开下载流
            GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(gridFSFile.getObjectId());
            GridFsResource gridFsResource = new GridFsResource(gridFSFile, gridFSDownloadStream);
            InputStream inputStream = gridFsResource.getInputStream();
            return inputStream;
        } catch (Exception e) {
            LOGGER.error("获取输入流错误 htmlFileId:{},错误:{}", htmlFileId, e);
        }
        return null;
    }

    /**
     * 根据页面id查询
     *
     * @param id
     * @return
     */
    public CmsPage findById(String id) {
        Optional<CmsPage> result = cmsPageRepository.findById(id);
        if (result.isPresent()) {
            return result.get();
        }
        return null;
    }
}
