package com.xuecheng.manage_cms_client.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @ClassName : RabbitmqConfiguration
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/16 16:11
 */
@Configuration
public class RabbitmqConfiguration {

    @Autowired
    private Environment env;
    //队列bean的名称
    public static final String QUEUE_CMS_POSTPAGE = "queue_cms_postpage";
    //交换机bean的名称
    public static final String EX_ROUTING_CMS_POSTPAGE = "ex_routing_cms_postpage";
    //队列的名称
    String queue_cms_postpage_name;
    //routingKey
    String routingKey;

    /**
     * 声明交换机
     *
     * @return
     */
    @Bean(EX_ROUTING_CMS_POSTPAGE)
    public Exchange EX_ROUTING_CMS_POSTPAGE(){
        return ExchangeBuilder.directExchange(EX_ROUTING_CMS_POSTPAGE).build();
    }

    /**
     * 声明队列
     *
     * @return
     */
    @Bean(QUEUE_CMS_POSTPAGE)
    public Queue QUEUE_CMS_POSTPAGE(){
        queue_cms_postpage_name = env.getProperty("xuecheng.mq.queue");
        Queue queue = new Queue(queue_cms_postpage_name, true);
        return queue;
    }

    /**
     * 队列和交换机绑定
     *
     * @param exchange
     * @param queue
     * @return
     */
    @Bean
    public Binding BINDING_QUEUE_INFORM_SMS(@Qualifier(EX_ROUTING_CMS_POSTPAGE) Exchange exchange,
                                            @Qualifier(QUEUE_CMS_POSTPAGE) Queue queue){
        routingKey = env.getProperty("xuecheng.mq.routingKey");
        return BindingBuilder.bind(queue).to(exchange).with(routingKey).noargs();
    }
}
