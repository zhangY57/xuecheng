package com.xuecheng.manage_cms_client.config;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @ClassName : GridFsConfiguration
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/17 10:59
 */
@Configuration
public class GridFsConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public GridFSBucket gridFSBucket(MongoClient mongoClient){
        String db = env.getProperty("spring.data.mongodb.database");
        MongoDatabase database = mongoClient.getDatabase(db);
        return GridFSBuckets.create(database);
    }
}
