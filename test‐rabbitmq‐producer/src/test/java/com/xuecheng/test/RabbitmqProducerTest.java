package com.xuecheng.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName : RabbitmqProducerTest
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/14 22:13
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class RabbitmqProducerTest {
    private static final String QUEUE_INFORM_EMAIL = "queue_inform_email_test";
    private static final String QUEUE_INFORM_SMS = "queue_inform_sms_test";
    private static final String EXCHANGE_FANOUT_INFORM = "exchange_topic_inform_test";
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void test1() {
        for (int i=0;i<=5;i++){
            String message = "send sms.email"+System.currentTimeMillis();
            rabbitTemplate.convertAndSend(EXCHANGE_FANOUT_INFORM,"inform.sms.email",message);
            System.out.println("发送的消息"+message);
        }
    }
}
