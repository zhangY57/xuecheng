package com.xuecheng.framework.exception;

import java.io.Serializable;

/**
 * @ClassName : KnownException
 * @Description : 自定义异常
 * @Auther : zhangyan
 * @Date :  2019/03/07 21:19
 */
public class KnownException extends Throwable implements Serializable {
    private static final long serialVersionUID = -8471201995271839633L;

    private String code;
    private String message;

    public KnownException(String code,String message){
        this.code = code;
        this.message =message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
