package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.CmsPage;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * cmspage接口
 */
public interface CmsPageRepository extends MongoRepository<CmsPage,String> {

    /**
     * 根据站点id和页面名称和页面路径
     * @param siteId
     * @param pageName
     * @param pageWebPath
     * @return
     */
    CmsPage findBySiteIdAndPageNameAndPageWebPath(String siteId,String pageName,String pageWebPath);
}
