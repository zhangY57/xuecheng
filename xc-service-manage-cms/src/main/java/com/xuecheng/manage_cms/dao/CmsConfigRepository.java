package com.xuecheng.manage_cms.dao;

import com.xuecheng.framework.domain.cms.model.CmsConfig;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *页面配置
 */
public interface CmsConfigRepository extends MongoRepository<CmsConfig,String> {
}
