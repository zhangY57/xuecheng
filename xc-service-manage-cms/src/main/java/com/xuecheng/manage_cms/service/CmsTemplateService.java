package com.xuecheng.manage_cms.service;

import com.xuecheng.framework.domain.cms.model.CmsTemplate;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;

import java.util.List;

public interface CmsTemplateService {

    /**
     * 查询列表
     * @return
     */
    EnvisionResponse<List<CmsTemplate>>findList();
}
