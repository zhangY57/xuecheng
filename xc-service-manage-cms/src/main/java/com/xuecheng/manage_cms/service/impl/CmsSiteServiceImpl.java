package com.xuecheng.manage_cms.service.impl;

import com.xuecheng.framework.domain.cms.CmsSite;
import com.xuecheng.framework.domain.cms.constants.Constants;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.manage_cms.dao.CmsSiteRepository;
import com.xuecheng.manage_cms.service.CmsSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName : CmsSiteServiceImpl
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/03 18:10
 */
@Service
public class CmsSiteServiceImpl implements CmsSiteService {

    @Autowired
    private CmsSiteRepository cmsSiteRepository;

    @Override
    public EnvisionResponse<List<CmsSite>> findList() {
        List<CmsSite> cmsSites = cmsSiteRepository.findAll();
        return new EnvisionResponse<>(Constants.SUCCESS,"查询成功",cmsSites);
    }
}
