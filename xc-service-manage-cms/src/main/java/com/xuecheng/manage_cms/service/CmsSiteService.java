package com.xuecheng.manage_cms.service;

import com.xuecheng.framework.domain.cms.CmsSite;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;

import java.util.List;

public interface CmsSiteService {

    /**
     * 查询列表
     * @return
     */
    EnvisionResponse<List<CmsSite>>findList();
}
