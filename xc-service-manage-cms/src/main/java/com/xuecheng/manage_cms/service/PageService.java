package com.xuecheng.manage_cms.service;

import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.request.PageQuery;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.domain.cms.response.Pager;
import com.xuecheng.framework.exception.KnownException;

public interface PageService {

    /**
     * 分页查询列表
     * @param page
     * @param size
     * @param pageQuery
     * @return
     */
    EnvisionResponse<Pager> findList(Integer page, Integer size, PageQuery pageQuery);

    /**
     * 新增页面
     * @param cmsPage
     * @return
     */
    EnvisionResponse<Boolean> saveCmsPage(CmsPage cmsPage);

    /**
     * 查询页面
     * @param id
     * @return
     */
    EnvisionResponse<CmsPage> findById(String id);

    /**
     * 修改
     * @param cmsPage
     * @return
     */
    EnvisionResponse<Boolean> updateCmsPage(String id, CmsPage cmsPage);

    /**
     * 删除
     * @param id
     * @return
     */
    EnvisionResponse<Boolean> deleteCmsPage(String id);

    /**
     * 页面静态化
     * @param pageId
     * @return
     */
    EnvisionResponse<String> getPageHtml(String pageId) throws KnownException;
}
