package com.xuecheng.manage_cms.service.impl;

import com.xuecheng.framework.domain.cms.constants.Constants;
import com.xuecheng.framework.domain.cms.model.CmsConfig;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.exception.KnownException;
import com.xuecheng.manage_cms.dao.CmsConfigRepository;
import com.xuecheng.manage_cms.service.CmsConfigService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @ClassName : CmsConfigServiceImpl
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/09 16:58
 */
@Service
public class CmsConfigServiceImpl implements CmsConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmsConfigServiceImpl.class);

    @Autowired
    private CmsConfigRepository cmsConfigRepository;

    /**
     * 根据业务主键
     * @param id
     * @return
     */
    @Override
    public EnvisionResponse<CmsConfig> queryCmsConfigByCode(String id) throws KnownException {
        if(StringUtils.isBlank(id)){
            throw new KnownException(Constants.NULLCODE,"业务主键不能为空");
        }
        try {
            Optional<CmsConfig> cmsConfigRepositoryById = cmsConfigRepository.findById(id);
            if(cmsConfigRepositoryById.isPresent()){
                return new EnvisionResponse<>(Constants.SUCCESS,"查询成功",cmsConfigRepositoryById.get());
            }
            throw new KnownException(Constants.NULLCODE,"未查询到页面配置信息");
        }catch (Exception e){
            LOGGER.error("查询出错 id:{}",id,e);
            throw new KnownException(Constants.ERRCODE,"查询出错");
        }

    }
}
