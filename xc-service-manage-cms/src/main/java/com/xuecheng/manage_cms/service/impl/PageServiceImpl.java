package com.xuecheng.manage_cms.service.impl;

import com.alibaba.fastjson.JSON;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.constants.Constants;
import com.xuecheng.framework.domain.cms.model.CmsTemplate;
import com.xuecheng.framework.domain.cms.request.PageQuery;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.domain.cms.response.Pager;
import com.xuecheng.framework.exception.KnownException;
import com.xuecheng.manage_cms.dao.CmsPageRepository;
import com.xuecheng.manage_cms.dao.CmsTemplateRepository;
import com.xuecheng.manage_cms.service.PageService;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @ClassName : PageServiceImpl
 * @Description : 页面Service
 * @Auther : zhangyan
 * @Date :  2019/02/21 21:59
 */
@Service
public class PageServiceImpl implements PageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PageServiceImpl.class);

    @Autowired
    private CmsPageRepository cmsPageRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private GridFSBucket gridFSBucket;

    @Autowired
    private CmsTemplateRepository cmsTemplateRepository;

    @Autowired
    private GridFsTemplate gridFsTemplate;

    /**
     * 分页查询列表
     *
     * @param page
     * @param size
     * @param pageQuery
     * @return
     */
    @Override
    public EnvisionResponse<Pager> findList(Integer page, Integer size, PageQuery pageQuery) {
        if (page == null || size == null) {
            return new EnvisionResponse<>(Constants.NULLCODE, "缺少分页参数");
        }
        try {
            ExampleMatcher exampleMatcher = ExampleMatcher.matching().
                    withMatcher("pageAliase", ExampleMatcher.GenericPropertyMatchers.contains());
            //封装查询条件
            CmsPage cmsPage = new CmsPage();
            if (pageQuery != null) {
                if (StringUtils.isNotBlank(pageQuery.getSiteId())) {
                    cmsPage.setSiteId(pageQuery.getSiteId());
                }
                if (StringUtils.isNotBlank(pageQuery.getPageAliase())) {
                    cmsPage.setPageAliase(pageQuery.getPageAliase());
                }
                if (StringUtils.isNotBlank(pageQuery.getTemplateId())) {
                    cmsPage.setTemplateId(pageQuery.getTemplateId());
                }
            }
            Example<CmsPage> example = Example.of(cmsPage, exampleMatcher);
            page -= 1;
            PageRequest pageable = PageRequest.of(page, size);
            Page<CmsPage> pages = cmsPageRepository.findAll(example, pageable);
            List<CmsPage> cmsPages = pages.getContent();
            long total = pages.getTotalElements();
            Pager<CmsPage> pager = new Pager<>();
            pager.setData(cmsPages);
            pager.setPageNo(page);
            pager.setTotalCount(total);
            return new EnvisionResponse<>(Constants.SUCCESS, "查询成功", pager);
        } catch (Exception e) {
            LOGGER.error("条件查询出错 参数 pageQuery:{}", JSON.toJSONString(pageQuery), e);
            return new EnvisionResponse<>(Constants.ERRCODE, "系统错误");
        }
    }

    /**
     * 保存页面
     *
     * @param cmsPage
     * @return
     */
    @Override
    public EnvisionResponse<Boolean> saveCmsPage(CmsPage cmsPage) {
        if (cmsPage == null) {
            return new EnvisionResponse<>(Constants.NULLCODE, "参数为空");
        }
        try {
            CmsPage cmsPage1 = cmsPageRepository.findBySiteIdAndPageNameAndPageWebPath(cmsPage.getSiteId(),
                    cmsPage.getPageName(), cmsPage.getPageWebPath());
            if (cmsPage1 != null) {
                return new EnvisionResponse<>(Constants.ERRCODE, "新增失败", false);
            }
            cmsPageRepository.save(cmsPage);
            return new EnvisionResponse<>(Constants.SUCCESS, "新增成功", true);
        } catch (Exception e) {
            LOGGER.error("条件查询出错 参数 pageQuery:{}", JSON.toJSONString(cmsPage), e);
            return new EnvisionResponse<>(Constants.ERRCODE, "新增错误");
        }

    }

    /**
     * 查询
     *
     * @param id
     * @return
     */
    @Override
    public EnvisionResponse<CmsPage> findById(String id) {
        Optional<CmsPage> result = cmsPageRepository.findById(id);
        if (result.isPresent()) {
            return new EnvisionResponse<>(Constants.SUCCESS, "查询成功", result.get());
        }
        return new EnvisionResponse<>(Constants.NULLCODE, "没有查询到");
    }

    /**
     * 更新页面信息
     *
     * @param id
     * @param cmsPage
     * @return
     */
    public EnvisionResponse<Boolean> updateCmsPage(String id, CmsPage cmsPage) {
        //根据id查询页面信息
        CmsPage one = this.findById(id).getData();
        if (one != null) {
            //更新模板id
            one.setTemplateId(cmsPage.getTemplateId());
            //更新所属站点
            one.setSiteId(cmsPage.getSiteId());
            //更新页面别名
            one.setPageAliase(cmsPage.getPageAliase());
            //更新页面名称
            one.setPageName(cmsPage.getPageName());
            //更新访问路径
            one.setPageWebPath(cmsPage.getPageWebPath());
            //更新物理路径
            one.setPagePhysicalPath(cmsPage.getPagePhysicalPath());
            //更新数据模型
            one.setDataUrl(cmsPage.getDataUrl());
            //执行更新
            CmsPage save = cmsPageRepository.save(one);
            if (save != null) {
                return new EnvisionResponse<>(Constants.SUCCESS, "更新成功", true);
            }
        }
        //返回失败
        return new EnvisionResponse<>(Constants.ERRCODE, "更新失败", false);
    }

    /**
     * 删除页面
     *
     * @param id
     * @return
     */
    @Override
    public EnvisionResponse<Boolean> deleteCmsPage(String id) {
        cmsPageRepository.deleteById(id);
        return new EnvisionResponse<>(Constants.SUCCESS, "删除成功", true);
    }

    /**
     * 页面静态化
     *
     * @param pageId
     * @return
     */
    public EnvisionResponse<String> getPageHtml(String pageId) throws KnownException {
        if (StringUtils.isBlank(pageId)) {
            throw new KnownException(Constants.NULLCODE, "业务主键不能为空");
        }
        //根据code查询cmsPage获取dataUrl
        Optional<CmsPage> optionalCmsPage = cmsPageRepository.findById(pageId);
        //获取远程数据model
        if (!optionalCmsPage.isPresent()) {
            throw new KnownException(Constants.NULLCODE, "未查询到页面信息");
        }
        CmsPage cmsPage = optionalCmsPage.get();
        Map<String, Object> model = this.getModel(cmsPage.getDataUrl());
        //获取页面模板内容
        String content = this.getTempalte(cmsPage.getTemplateId());
        //执行页面静态化
        String html = this.generateHtml(content, model);
        if (StringUtils.isBlank(html)) {
            throw new KnownException(Constants.NULLCODE, "html为空");
        }
        return new EnvisionResponse<>(Constants.SUCCESS, "页面静态化成功", html);
    }

    /**
     * 获取模板
     *
     * @param templateCode
     * @return
     * @throws KnownException
     */
    private String getTempalte(String templateCode) throws KnownException {
        if (StringUtils.isBlank(templateCode)) {
            throw new KnownException(Constants.NULLCODE, "模板code不能为空");
        }
        Optional<CmsTemplate> byId = cmsTemplateRepository.findById(templateCode);
        if (!byId.isPresent()) {
            throw new KnownException(Constants.NULLCODE, "未查询到模板信息");
        }
        //取模板内容
        String templateFileId = byId.get().getTemplateFileId();
        if (!byId.isPresent()) {
            throw new KnownException(Constants.NULLCODE, "未查询到模板文件信息");
        }
        //查询文件
        GridFSFile gridFSFile = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(templateFileId)));

        //打开下载流
        GridFSDownloadStream stream = gridFSBucket.openDownloadStream(gridFSFile.getObjectId());
        //创建gridFsResource
        GridFsResource gridFsResource = new GridFsResource(gridFSFile, stream);
        //获取流文件
        String content = null;
        try {
            content = IOUtils.toString(gridFsResource.getInputStream(), "utf-8");
        } catch (Exception e) {
            LOGGER.error("转换错误 错误:{}", e);
            throw new KnownException(Constants.ERRCODE, "系统错误");
        }
        return content;
    }

    /**
     * 生成模板
     *
     * @param template
     * @param model
     * @return
     * @throws KnownException
     */
    public String generateHtml(String template, Map<String, Object> model) throws KnownException {
        try {
            //版本
            Configuration configuration = new Configuration(Configuration.getVersion());
            //模板加载器
            StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
            //加载模板
            stringTemplateLoader.putTemplate("template", template);
            configuration.setTemplateLoader(stringTemplateLoader);
            Template template1 = configuration.getTemplate("template", "utf-8");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(template1, model);
            return html;
        } catch (Exception e) {
            LOGGER.error("生成模板错误 错误:{}", e);
            throw new KnownException(Constants.ERRCODE, "生成模板错误");
        }
    }

    /**
     * 获取模型数据
     *
     * @param dataUrl
     * @return
     */
    private Map<String, Object> getModel(String dataUrl) throws KnownException {
        if (StringUtils.isBlank(dataUrl)) {
            throw new KnownException(Constants.NULLCODE, "远程地址不能空");
        }
        try {
            ResponseEntity<Map> forEntity = restTemplate.getForEntity(dataUrl, Map.class);
            Map map = forEntity.getBody();
            return map;
        } catch (Exception e){
            LOGGER.error("连接异常,请检查地址 dataUrl:{}",dataUrl, e);
            throw new KnownException(Constants.ERRCODE, "连接异常,请检查地址");
        }
    }
}
