package com.xuecheng.manage_cms.service.impl;

import com.xuecheng.framework.domain.cms.constants.Constants;
import com.xuecheng.framework.domain.cms.model.CmsTemplate;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.manage_cms.dao.CmsTemplateRepository;
import com.xuecheng.manage_cms.service.CmsTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName : CmsTemplateServiceImpl
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/03 18:10
 */
@Service
public class CmsTemplateServiceImpl implements CmsTemplateService {

    @Autowired
    private CmsTemplateRepository cmsTemplateRepository;

    @Override
    public EnvisionResponse<List<CmsTemplate>> findList() {
        List<CmsTemplate> cmsSites = cmsTemplateRepository.findAll();
        return new EnvisionResponse<>(Constants.SUCCESS,"查询成功",cmsSites);
    }
}
