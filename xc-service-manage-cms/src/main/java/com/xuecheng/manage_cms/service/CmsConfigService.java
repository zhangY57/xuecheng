package com.xuecheng.manage_cms.service;

import com.xuecheng.framework.domain.cms.model.CmsConfig;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.exception.KnownException;

public interface CmsConfigService {
    /**
     * 根据code查询
     * @param id
     * @return
     */
    EnvisionResponse<CmsConfig> queryCmsConfigByCode(String id) throws KnownException;
}
