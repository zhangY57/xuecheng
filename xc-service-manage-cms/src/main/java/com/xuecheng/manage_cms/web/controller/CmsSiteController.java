package com.xuecheng.manage_cms.web.controller;

import com.xuecheng.api.cms.CmsSiteControllerApi;
import com.xuecheng.framework.domain.cms.CmsSite;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.manage_cms.service.CmsSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName : CmsSiteController
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/03 18:12
 */
@RestController
@RequestMapping("/cms/cmsSite")
public class CmsSiteController implements CmsSiteControllerApi {
    @Autowired
    private CmsSiteService cmsSiteService;

    @RequestMapping("/findList")
    public EnvisionResponse<List<CmsSite>> findList(){
        return cmsSiteService.findList();
    }
}
