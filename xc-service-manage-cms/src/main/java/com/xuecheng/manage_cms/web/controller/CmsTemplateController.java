package com.xuecheng.manage_cms.web.controller;

import com.xuecheng.api.cms.CmsTemplateControllerApi;
import com.xuecheng.framework.domain.cms.model.CmsTemplate;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.manage_cms.service.CmsTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName : CmsTemplateController
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/03 18:12
 */
@RestController
@RequestMapping("/cms/cmsTemplate")
public class CmsTemplateController implements CmsTemplateControllerApi {
    @Autowired
    private CmsTemplateService cmsTemplateService;

    @RequestMapping("/cmsTemplateList")
    public EnvisionResponse<List<CmsTemplate>> findList(){
        return cmsTemplateService.findList();
    }
}
