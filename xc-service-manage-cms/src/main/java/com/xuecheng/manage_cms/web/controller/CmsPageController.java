package com.xuecheng.manage_cms.web.controller;

import com.xuecheng.api.cms.CmsPageControllerApi;
import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.request.PageQuery;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.domain.cms.response.Pager;
import com.xuecheng.framework.exception.KnownException;
import com.xuecheng.manage_cms.service.PageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName : CmsPageController
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/02/20 21:31
 */
@Controller
@RequestMapping("/cms")
public class CmsPageController implements CmsPageControllerApi {

    @Autowired
    private PageService pageService;

    @Autowired
    private static final Logger LOGGER = LoggerFactory.getLogger(CmsPageController.class);

    @GetMapping("/list/{page}/{size}")
    @ResponseBody
    @Override
    public EnvisionResponse<Pager> findList(@PathVariable("page") Integer page,
                                            @PathVariable("size") Integer size, PageQuery pageQuery) {
        EnvisionResponse<Pager> pageList = pageService.findList(page, size, pageQuery);
        return pageList;
    }

    /**
     * 新增
     * @param cmsPage
     * @return
     */
    @PostMapping("/saveCmsPage")
    @ResponseBody
    @Override
    public EnvisionResponse<Boolean> saveCmsPage(@RequestBody CmsPage cmsPage){
        EnvisionResponse<Boolean> booleanEnvisionResponse = pageService.saveCmsPage(cmsPage);
        return booleanEnvisionResponse;
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Override
    @ResponseBody
    @GetMapping("/findById/{id}")
    public EnvisionResponse<CmsPage> findById(@PathVariable("id") String id) {
        return pageService.findById(id);
    }

    /**
     * 修改页面
     * @param cmsPage
     * @return
     */
    @Override
    @ResponseBody
    @PostMapping("/updateCmsPage/{id}")
    public EnvisionResponse<Boolean> updateCmsPage(@PathVariable("id") String id,@RequestBody CmsPage cmsPage) {
        return pageService.updateCmsPage(id,cmsPage);
    }

    /**
     * 根据id删除
     * @param id
     * @return
     */
    @Override
    @ResponseBody
    @GetMapping("/updateCmsPage/{id}")
    public EnvisionResponse<Boolean> deleteCmsPage(@PathVariable("id") String id) {
       return pageService.deleteCmsPage(id);
    }

    /**
     * 页面静态化预览
     * @param cmsPageId
     */
    @RequestMapping(value = "/preview/{cmsPageId}",method = RequestMethod.GET)
    public void generater(@PathVariable("cmsPageId") String cmsPageId, HttpServletResponse response){
        try {
            EnvisionResponse<String> html = pageService.getPageHtml(cmsPageId);
            String content = html.getData();
            //写入浏览器
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(content.getBytes("utf-8"));
        } catch (KnownException e) {
            LOGGER.error("静态化预览错误 错误:{}",e);
        } catch (Exception e){
            LOGGER.error("静态化预览错误 错误:{}",e);
        }
    }
}
