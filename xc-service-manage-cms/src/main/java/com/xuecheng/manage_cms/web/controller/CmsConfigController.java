package com.xuecheng.manage_cms.web.controller;

import com.xuecheng.api.cms.CmsConfigControllerApi;
import com.xuecheng.framework.domain.cms.constants.Constants;
import com.xuecheng.framework.domain.cms.model.CmsConfig;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.exception.KnownException;
import com.xuecheng.manage_cms.service.CmsConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName : CmsConfigController
 * @Description : 页面管理控制类
 * @Auther : zhangyan
 * @Date :  2019/03/09 17:17
 */
@RestController
public class CmsConfigController implements CmsConfigControllerApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmsConfigController.class);

    @Autowired
    private CmsConfigService cmsConfigService;

    @Override
    @GetMapping("/cms/cmsConfig/queryConfigById/{id}")
    public EnvisionResponse<CmsConfig> queryCmsConfigById(@PathVariable("id") String id) {
        try {
            return cmsConfigService.queryCmsConfigByCode(id);
        } catch (Exception e) {
            LOGGER.error("查询页面配置信息出错 id:{},错误:{}", id, e);
            return new EnvisionResponse(Constants.SYSERRCODE, "请求超时,请稍后再试");
        } catch (KnownException e) {
            LOGGER.error("查询页面配置信息出错 id:{},错误:{}", id, e);
            return new EnvisionResponse(e.getCode(), e.getMessage());
        }
    }
}
