package com.xuecheng.manage_cms.config;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @ClassName : GridFsTemplateConfiguration
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/09 22:43
 */
@Configuration
public class GridFsTemplateConfiguration {
    @Autowired
    private Environment env;

    @Bean
    public GridFSBucket getGridFSBucket(MongoClient mongoClient){
        String db = env.getProperty("spring.data.mongodb.database");
        MongoDatabase database = mongoClient.getDatabase(db);
        return GridFSBuckets.create(database);
    }
}
