package com.xuecheng.manage_cms;

import com.alibaba.fastjson.JSON;
import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.exception.KnownException;
import com.xuecheng.manage_cms.dao.CmsPageRepository;
import com.xuecheng.manage_cms.service.PageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

/**
 * @ClassName : Test1
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/03 17:30
 */
@SpringBootTest()
@RunWith(SpringJUnit4ClassRunner.class)
public class Test1 {
    @Autowired
    private CmsPageRepository cmsPageRepository;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private PageService pageService;

    /**
     * 测试生成html
     */
    @Test
    public void test3() throws KnownException {
        EnvisionResponse<String> pageHtml = pageService.getPageHtml("string");
        System.out.println("******************************");
        System.out.println(pageHtml.getData());
    }

    /**
     * 测试restTemplate
     */
    @Test
    public void test1(){
        ResponseEntity<Map> forEntity = restTemplate.
                getForEntity("http://localhost:31001/cms/cmsConfig/queryConfigById/5a791725dd573c3574ee333f", Map.class);
        HttpStatus statusCode = forEntity.getStatusCode();
        Map map= forEntity.getBody();
        System.out.println("********************************");
        System.out.println(JSON.toJSONString(map));
        System.out.println(JSON.toJSONString(statusCode));

    }
    //自定义条件查询测试
    @Test
    public void testFindAllByExample() {
        //分页参数
        int page = 0;//从0开始
        int size = 10;
        Pageable pageable = PageRequest.of(page,size);

        //条件值对象
        CmsPage cmsPage= new CmsPage();
        //要查询5a751fab6abb5044e0d19ea1站点的页面
//        cmsPage.setSiteId("5b30b052f58b4411fc6cb1cf");
        //设置模板id条件
//        cmsPage.setTemplateId("5ad9a24d68db5239b8fef199");
        //设置页面别名
        cmsPage.setPageAliase("轮播");
        //条件匹配器
//        ExampleMatcher exampleMatcher = ExampleMatcher.matching();
//        exampleMatcher = exampleMatcher.withMatcher("pageAliase", ExampleMatcher.GenericPropertyMatchers.contains());
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withMatcher("pageAliase", ExampleMatcher.GenericPropertyMatchers.contains());
        //ExampleMatcher.GenericPropertyMatchers.contains() 包含关键字
//        ExampleMatcher.GenericPropertyMatchers.startsWith()//前缀匹配
        //定义Example
        Example<CmsPage> example = Example.of(cmsPage,exampleMatcher);
        Page<CmsPage> all = cmsPageRepository.findAll(example, pageable);
        List<CmsPage> content = all.getContent();
        System.out.println(content);
    }
}
