package com.xuecheng.manage_cms;

import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @ClassName : FreemarkerTest
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/09 14:50
 */
@SpringBootTest()
@RunWith(SpringRunner.class)
public class GridFsTest {

    @Autowired
    private GridFsTemplate gridFsTemplate;
    @Autowired
    private GridFSBucket gridFSBucket;

    @Test
    public void test1() throws Exception {
        FileInputStream fileInputStream = new FileInputStream(new File("D:\\workSpace\\XcEduCode01\\test-freemarker\\src\\main\\resources\\templates\\index_banner.ftl"));
        ObjectId obj = gridFsTemplate.store(fileInputStream, "张旭测试轮播模板", "");
        System.out.println("*******************************************");
        System.out.println(obj.toString());
    }

    @Test
    public void test2() throws Exception {
        String fileId = "5c83d81852b9911530072569";
        //查询文件
        GridFSFile gridFSFile = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(fileId)));
        //打开下载流
        GridFSDownloadStream stream = gridFSBucket.openDownloadStream(gridFSFile.getObjectId());
        //创建gridFsResource
        GridFsResource gridFsResource = new GridFsResource(gridFSFile,stream);
        //获取流文件
        IOUtils.copy(gridFsResource.getInputStream(),new FileOutputStream(new File("d:/下载.mp4")));
//        String s = IOUtils.toString(gridFsResource.getInputStream(), "utf-8");
//        System.out.println(s);
    }

    @Test
    public void test3(){
        gridFsTemplate.delete(Query.query(Criteria.where("_id").is("5c83d8af52b991222031c31e")));
    }
}
