package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.cms.CmsSite;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import io.swagger.annotations.ApiOperation;

import java.util.List;

/**
 * @ClassName : CmsSiteControllerApi
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/05 21:42
 */
public interface CmsSiteControllerApi {

    @ApiOperation("查询全部站点")
    EnvisionResponse<List<CmsSite>> findList();

}
