package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.cms.model.CmsConfig;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 页面配置暴露接口
 */
@Api(value = "页面配置管理",description = "cms配置管理接口,提供数据模型的管理,查询接口")
public interface CmsConfigControllerApi {

    @ApiOperation("根据id查询")
    EnvisionResponse<CmsConfig> queryCmsConfigById(String id);
}
