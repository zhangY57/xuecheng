package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.request.PageQuery;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import com.xuecheng.framework.domain.cms.response.Pager;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @ClassName : CmsPageControllerApi
 * @Description : 微服务远程调用
 * @Auther : zhangyan
 * @Date :  2019/02/20 20:25
 */
public interface CmsPageControllerApi {

    @ApiOperation("页面列表查询")
    EnvisionResponse<Pager> findList(Integer page, Integer size, PageQuery pageQuer);

    @ApiOperation("新增页面")
    EnvisionResponse<Boolean> saveCmsPage(@RequestBody CmsPage cmsPage);

    @ApiOperation("根据id查询页面")
    EnvisionResponse<CmsPage> findById(String id);

    @ApiOperation("修改页面")
    EnvisionResponse<Boolean> updateCmsPage(String id,CmsPage cmsPage);

    @ApiOperation("删除页面")
    EnvisionResponse<Boolean> deleteCmsPage(String id);
}
