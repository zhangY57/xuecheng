package com.xuecheng.api.cms;

import com.xuecheng.framework.domain.cms.model.CmsTemplate;
import com.xuecheng.framework.domain.cms.response.EnvisionResponse;
import io.swagger.annotations.ApiOperation;

import java.util.List;

/**
 * @ClassName : CmsSiteControllerApi
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/05 21:42
 */
public interface CmsTemplateControllerApi {

    @ApiOperation("查询全部接口")
    EnvisionResponse<List<CmsTemplate>> findList();

}
