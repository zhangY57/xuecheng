package com.xuecheng;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.Test;

/**
 * @ClassName : MongoTest
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/02/18 22:04
 */

public class MongoTest {

    @Test
    public void test1(){
        //创建客户端对象
//        MongoClientURI mongoClientURI = new MongoClientURI("mongodb://root:root@localhost:27017");
//        MongoClient mongoClient = new MongoClient(mongoClientURI);
        MongoClient mongoClient = new MongoClient("localhost",27017);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("test");
        MongoCollection<Document> aa = mongoDatabase.getCollection("aa");
        Document document = aa.find().first();
        String s = document.toJson();
        System.out.println(s);
    }
}
