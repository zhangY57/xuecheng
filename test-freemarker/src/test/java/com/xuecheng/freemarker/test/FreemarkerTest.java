package com.xuecheng.freemarker.test;

import com.test.freemarker.FreemakerApplication;
import com.test.freemarker.model.Student;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.*;

/**
 * @ClassName : FreemarkerTest
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/09 14:50
 */
@SpringBootTest(classes = FreemakerApplication.class)
@RunWith(SpringRunner.class)
public class FreemarkerTest {

    @Test
    public void generateHtml() throws Exception {
        Configuration configuration = new Configuration(Configuration.getVersion());
        String path = this.getClass().getResource("/").getPath();
        configuration.setDefaultEncoding("utf-8");
        configuration.setDirectoryForTemplateLoading(new File(path + "/templates/"));
        Template template = configuration.getTemplate("test1.ftl");
        Map<String, Object> map = this.getMap();
        String content = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
        //输出流
        FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/test1.html"));
        //输出流
        InputStream inputStream = IOUtils.toInputStream(content);
        int copy = IOUtils.copy(inputStream, fileOutputStream);
        System.out.println("**********************************");
        System.out.println(content);
        System.out.println(copy);
    }

    @Test
    public void stringGenerateHtml() throws Exception{
        StringBuffer buffer = new StringBuffer();
        buffer.append("<!DOCTYPE html>\n");
        buffer.append("<html>\n");
        buffer.append("<head>\n");
        buffer.append("<body>\n");
        buffer.append("Hello ${name}\n");
        buffer.append("</body>\n");
        buffer.append("</head>\n");
        buffer.append("</html>\n");
        //指定版本
        Configuration configuration = new Configuration(Configuration.getVersion());
        //模板加载
        StringTemplateLoader stringTemplateLoader = new StringTemplateLoader();
        stringTemplateLoader.putTemplate("template",buffer.toString());
        configuration.setTemplateLoader(stringTemplateLoader);
        Template template = configuration.getTemplate("template", "utf-8");
        Map map = new HashMap();
        map.put("name","老马程序员");
        String content = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
        //输入流
        InputStream inputStream = IOUtils.toInputStream(content);
        //输出流
        FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/test2.html"));
        int copy = IOUtils.copy(inputStream, fileOutputStream);

    }

    private Map<String, Object> getMap() {
        Map map = new HashMap();
        //向数据模型放数据
        map.put("name","黑马程序员");
        //第一个实体类
        Student student1 = new Student();
        student1.setName("小明");
        student1.setAge(18);
        student1.setMoney(1000.86f);
        student1.setBirthday(new Date());
        //第二个实体类
        Student student2 = new Student();
        student2.setName("小红");
        student2.setMoney(200.1f);
        student2.setAge(19);
        student2.setBirthday(new Date());

        List<Student> friends = new ArrayList<>();
        friends.add(student1);
        student2.setFriends(friends);
        student2.setBestFriend(student1);
        List<Student> stus = new ArrayList<>();
        stus.add(student1);
        stus.add(student2);
        //向数据模型放数据
        map.put("stus",stus);
        //准备map数据
        HashMap<String, Student> stuMap = new HashMap<>();
        stuMap.put("stu1", student1);
        stuMap.put("stu2", student2);
        //向数据模型放数据
        map.put("stu1", student1);
        //向数据模型放数据
        map.put("stuMap", stuMap);
        return map;
    }
}
