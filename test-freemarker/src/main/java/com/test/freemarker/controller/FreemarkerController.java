package com.test.freemarker.controller;

import com.test.freemarker.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * @ClassName : FreemarkerController
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/08 22:55
 */
@Controller
public class FreemarkerController {
    @Autowired
    private RestTemplate restTemplate;


    @RequestMapping("/freemarker/indexBanner")
    public String index_banner(Map<String,Object> map){
        ResponseEntity<Map> forEntity = restTemplate.getForEntity
                ("http://localhost:31001/cms/cmsConfig/queryConfigById/5a791725dd573c3574ee333f", Map.class);
        Map body = forEntity.getBody();
        map.putAll(body);
        return "index_banner";
    }

    @RequestMapping("/freemarker/test1")
    public String freemarkerTest(Map<String,Object> map){
         //向数据模型放数据
        map.put("name","黑马程序员");
        //第一个实体类
        Student student1 = new Student();
        student1.setName("小明");
        student1.setAge(18);
        student1.setMoney(1000.86f);
        student1.setBirthday(new Date());
        //第二个实体类
        Student student2 = new Student();
        student2.setName("小红");
        student2.setMoney(200.1f);
        student2.setAge(19);
        student2.setBirthday(new Date());

        List<Student> friends = new ArrayList<>();
        friends.add(student1);
        student2.setFriends(friends);
        student2.setBestFriend(student1);
        List<Student> stus = new ArrayList<>();
        stus.add(student1);
        stus.add(student2);
        //向数据模型放数据
        map.put("stus",stus);
        //准备map数据
        HashMap<String, Student> stuMap = new HashMap<>();
        stuMap.put("stu1", student1);
        stuMap.put("stu2", student2);
        //向数据模型放数据
        map.put("stu1", student1);
        //向数据模型放数据
        map.put("stuMap", stuMap);
        //返回模板文件名称
        return "test1";
    }
}
