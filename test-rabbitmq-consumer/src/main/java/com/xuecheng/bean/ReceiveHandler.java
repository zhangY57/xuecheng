package com.xuecheng.bean;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName : ReceiveHandler
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/16 11:09
 */
@Component
public class ReceiveHandler {
    private static final String QUEUE_INFORM_EMAIL = "queue_inform_email_test";
    private static final String QUEUE_INFORM_SMS = "queue_inform_sms_test";
    private static final String EXCHANGE_FANOUT_INFORM="exchange_topic_inform_test";

    @RabbitListener(queues = {QUEUE_INFORM_EMAIL})
    public void test1(String msg, Message message, Channel channel){
        System.out.println(msg);
    }

    @RabbitListener(queues = {QUEUE_INFORM_SMS})
    public void test2(String msg, Message message, Channel channel){
        System.out.println(msg);
    }
}
