package com.xuecheng.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

/**
 * @ClassName : RabbitmqConfiguration
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/16 10:56
 */
//@Configuration
public class RabbitmqConfiguration {
    private static final String QUEUE_INFORM_EMAIL = "queue_inform_email_test";
    private static final String QUEUE_INFORM_SMS = "queue_inform_sms_test";
    private static final String EXCHANGE_FANOUT_INFORM="exchange_topic_inform_test";

    //配置交换机
    @Bean(EXCHANGE_FANOUT_INFORM)
    public Exchange exchange(){
        return ExchangeBuilder.topicExchange(EXCHANGE_FANOUT_INFORM).build();
    }

    //配置队列
    @Bean(QUEUE_INFORM_EMAIL)
    public Queue emailQueue(){
        Queue queue = new Queue(QUEUE_INFORM_EMAIL,true);
        return queue;
    }
    //配置队列
    @Bean(QUEUE_INFORM_SMS)
    public Queue smsQueue(){
        Queue queue = new Queue(QUEUE_INFORM_SMS,true);
        return queue;
    }

    /**
     * channel.queueBind(QUEUE_INFORM_SMS,EXCHANGE_FANOUT_INFORM,"");
     * 绑定交换机
     */
    @Bean
    public Binding bindingEmail(@Qualifier(value = QUEUE_INFORM_EMAIL) Queue queue,
                                @Qualifier(EXCHANGE_FANOUT_INFORM) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("inform.#.email.#").noargs();
    }

    @Bean
    public Binding bindingSms(@Qualifier(value = QUEUE_INFORM_SMS) Queue queue,
                              @Qualifier(EXCHANGE_FANOUT_INFORM) Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("inform.#.sms.#").noargs();
    }

}
