package com.xuecheng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName : RabbitmqTest
 * @Description :
 * @Auther : zhangyan
 * @Date :  2019/03/14 22:11
 */
@SpringBootApplication
public class RabbitmqTest {
    public static void main(String[] args) {
        SpringApplication.run(RabbitmqTest.class);
    }
}
